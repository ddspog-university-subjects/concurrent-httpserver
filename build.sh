#!/bin/bash
# Constants
OUT_DIR=./out
PROD_DIR=/production
PROJ_DIR=/fpc-web-server
ANY="/*"

# Clean bin folder
if [ ! -d $OUT_DIR ]; then
  mkdir $OUT_DIR
fi
if [ ! -d $OUT_DIR$PROD_DIR ]; then
  mkdir $OUT_DIR$PROD_DIR
fi
if [ ! -d $OUT_DIR$PROD_DIR$PROJ_DIR ]; then
  mkdir $OUT_DIR$PROD_DIR$PROJ_DIR
fi

rm -R $OUT_DIR$PROD_DIR$PROJ_DIR$ANY

# Build
javac -d $OUT_DIR$PROD_DIR$PROJ_DIR $(find . -name \*.java)
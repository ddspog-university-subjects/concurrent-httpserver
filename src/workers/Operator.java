package workers;

import elements.HttpRequest;
import elements.HttpResponse;
import parser.RequestParser;
import parser.ResponseParser;
import server.Manager;

import java.io.IOException;

/**
 * Represents a worker for this server, with the function to attend one request.
 */
@SuppressWarnings({"FieldCanBeLocal", "WeakerAccess"})
public class Operator implements Runnable {

    private final Manager manager;
    private final boolean IGNORE_NULL_REQUESTS = true;

    /**
     * Main constructor.
     *
     * @param manager the manager controlling this operator.
     */
    public Operator(Manager manager) {
        this.manager = manager;
    }

    /**
     * Run operator functions.
     */
    @SuppressWarnings("ConstantConditions")
    @Override
    public void run() {
        try (ClientConnection con = new ClientConnection(this.manager.getServer())) {
            HttpRequest request = RequestParser.parseRequest(con);
            answer(request, con);
        } catch (IOException e) {
            if (!Thread.interrupted()) {
                System.out.println(e.getMessage());
            }
        } catch (NullPointerException e) {
            if (!this.IGNORE_NULL_REQUESTS) {
                System.out.println("Received a null request. Ignoring it.");
            }
        }
        this.manager.notifyVacancy();
    }

    /**
     * Answer a HttpRequest.
     *
     * @param request the http request to answer.
     * @param con     the connection that received the request.
     */
    private void answer(HttpRequest request, ClientConnection con) {
        HttpResponse response = ResponseParser.parseRequest(request);
        con.out.println(response.toString());
        con.out.flush();
    }
}

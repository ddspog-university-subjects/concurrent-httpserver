package workers;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import exceptions.CloseConnectionException;
import exceptions.OpenConnectionException;

@SuppressWarnings("WeakerAccess")
public class ClientConnection implements AutoCloseable {

	public Socket clientSocket;
	public BufferedReader in;
	public PrintWriter out;

	public ClientConnection(ServerSocket serverSocket) throws IOException {
		try {
			this.clientSocket = serverSocket.accept();
			this.in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			this.out = new PrintWriter(clientSocket.getOutputStream(), true);	
		} catch (IOException e) {
			throw new OpenConnectionException("Opening Server connection failed: " + e.getMessage());
		}
		
	}
	
	@Override
	public void close() throws IOException {
		try {
			this.clientSocket.close();
			this.in.close();
			this.out.close();	
		} catch (IOException e) {
			throw new CloseConnectionException("Closing Server connection failed: " + e.getMessage());
		}
	}
	
}
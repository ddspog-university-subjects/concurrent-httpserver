package elements;

import java.util.HashMap;
import java.util.Map;

/**
 * Class representing any Http responses. It stores information on a easy way to access important data.
 */
@SuppressWarnings({"WeakerAccess", "FieldCanBeLocal"})
public class HttpResponse {

    private final String CRLF = "\r\n";
    private final String SP = " ";

    // Main attributes, that are needed for execution of most methods.
    private String version;
    private String status;
    private String reason;
    private String body;

    // Map of headers for fast research.
    private Map<String, String> headers;

    /**
     * Main constructor.
     */
    public HttpResponse() {
        this.body = "";
        this.headers = new HashMap<>();
    }

    /**
     * Get the version of http on request.
     * @return the version of http on request.
     */
    @SuppressWarnings("unused")
    public String getVersion() {
        return version;
    }

    /**
     * Set the version of http on request.
     * @param version the version of http on request.
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * Get the status of http request result.
     * @return the status of http request result.
     */
    @SuppressWarnings("unused")
    public String getStatus() {
        return status;
    }

    /**
     * Set the status of http request result.
     * @param status the status of http request result.
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Get the status reason of http request results.
     * @return the status reason of http request results.
     */
    @SuppressWarnings("unused")
    public String getReason() {
        return reason;
    }

    /**
     * Set the status reason of http request results.
     * @param reason the status reason of http request results.
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * Add an header to the headers map. It uses the header name as key.
     * @param name the name of the header.
     * @param value the value of the header.
     */
    public void addHeader(String name, String value) {
        this.headers.put(name, value);
    }

    /**
     * Consult header value by its name.
     * @param key name of header to consult.
     * @return header value.
     */
    public String getHeader(String key){
        return this.headers.getOrDefault(key, "");
    }

    /**
     * Consult if certain header it's present on request.
     * @param key name of header to consult.
     * @return whether header it's present or not.
     */
    public boolean hasHeader(String key) {
        return this.headers.containsKey(key);
    }

    /**
     * Add line to body string.
     * @param line new line to add on body.
     */
    @SuppressWarnings("unused")
    public void addToBody(String line) {
        this.body += line;
    }

    /**
     * Get whole body as string.
     * @return the body added on request.
     */
    public String getBody(){
        return this.body;
    }

    /**
     * Converts the response into a string.
     * @return the response as a string.
     */
    @Override
    public String toString() {
        String response = "";
        String statusLine = this.version + SP + this.status + SP + this.reason + CRLF;

        response += statusLine;

        StringBuilder headerSection = new StringBuilder();
        for (String headerName : this.headers.keySet()){
            String headerLine = headerName + ":" + SP + this.getHeader(headerName) + CRLF;
            headerSection.append(headerLine);
        }

        response += headerSection.toString();

        if(this.hasHeader("Content-Length") || this.hasHeader("Transfer-Encoding")){
            response += CRLF + this.getBody();
        }

        return response;
    }

    public void setBody(String body) {
        this.body = body;
    }
}

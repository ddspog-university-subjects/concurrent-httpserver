package elements;

/**
 * Enum that represents status of connection. It helps to se for every involving part of the server, whether they are
 * able to operate or not.
 */
public enum ConnectionStatus {
	OPERATING, CLOSED
}
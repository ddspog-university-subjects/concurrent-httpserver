package elements;

import java.util.HashMap;
import java.util.Map;

/**
 * Class representing any Http requests. It stores information on a easy way to access important data.
 */
public class HttpRequest {

	// Main attributes, that are needed for execution of most methods.
	private String method;
	private String path;
	private String version;
	private String body;

	// Map of headers for fast research.
	private Map<String, String> headers;

	/**
	 * Main constructor.
	 */
	public HttpRequest() { 
		this.body = "";
		this.headers = new HashMap<>();
	}

	/**
	 * Get the method of request.
	 * @return the method of request.
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * Set the method of request.
	 * @param method the method of request.
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * Get the path given on request.
	 * @return the path given on request.
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Set the path given on request.
	 * @param path the path given on request.
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * Get the version of Http used on request.
	 * @return the version of Http used on request.
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Set the version of Http used on request.
	 * @param version the version of Http used on request.
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Add an header to the headers map. It uses the header name as key.
	 * @param line the header line to process.
	 */
	public void addHeader(String line) {
		String[] statement = line.split(": ");
		this.headers.put(statement[0], statement[1]);
	}

	/**
	 * Consult header value by its name.
	 * @param key name of header to consult.
	 * @return header value.
	 */
	@SuppressWarnings("unused")
	public String getHeader(String key){
		return this.headers.getOrDefault(key, "");
	}

	/**
	 * Consult if certain header it's present on request.
	 * @param key name of header to consult.
	 * @return whether header it's present or not.
	 */
	public boolean hasHeader(String key) {
		return this.headers.containsKey(key);
	}

	/**
	 * Add line to body string.
	 * @param line new line to add on body.
	 */
	public void addToBody(String line) {
		this.body += line;
	}

	/**
	 * Get whole body as string.
	 * @return the body added on request.
	 */
	@SuppressWarnings("unused")
	public String getBody(){
		return this.body;
	}
}

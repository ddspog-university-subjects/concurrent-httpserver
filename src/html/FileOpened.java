package html;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

/**
 * Open a file with security, and allows to read its content.
 */
@SuppressWarnings("WeakerAccess")
public class FileOpened implements AutoCloseable {

	private final Charset charset;
    private String filepath;
	private Path file;

    /**
     * Default constructor, opens the file.
     * @param filepath the path of the file to open.
     * @param charset the encoding configured for the file.
     * @throws IOException if the path is invalid.
     */
	public FileOpened(String filepath, Charset charset) throws IOException {
		try {
            this.filepath = filepath;
			this.file = Paths.get(filepath);
			this.charset = charset;
		} catch( Exception e ) {
			throw new IOException("Couldn't find file on path: " + this.filepath);
		}
	}

    /**
     * Get whole content of file in a String.
     * @return whole content of file in a String.
     * @throws IOException if the file can't be read.
     */
	public String getContent() throws IOException {
		try {
		    return Files.readAllLines(this.file, this.charset).stream().collect(Collectors.joining(""));
		} catch (IOException e) {
			throw new IOException("Couldn't read file on : " + this.filepath);
		}
	}

    /**
     * Closes the file.
     * @throws Exception if the file can't be closed.
     */
	@Override
	public void close() throws Exception {
	}
}

package html;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Manager of the documents involved in this server. Provides the files 'as is' to other classes.
 */
public class DocumentFinder {

    private static final String DEFAULT_FILE = "lib/index.html";

    /**
     * Get content of a HTML file in a String.
     * @param path the path of HTML file to read.
     * @param encoding the encoding of HTML file.
     * @return the HTML file as a String.
     * @throws IOException if the file can't be read.
     */
    public static String getHTML(String path, Charset encoding) throws IOException {
        String filePath = formatFilePath(path);
        if (filePath.equals("lib/")) {
            filePath = DEFAULT_FILE;
        }

        try ( FileOpened file = new FileOpened(filePath, encoding) ) {
            return file.getContent();
        } catch (Exception e) {
            throw new IOException(e.getMessage());
        }
    }

    /**
     * Format file path to be correctly read from Java.
     * @param path the path received to format.
     * @return the path in the directory where the public files are.
     */
    private static String formatFilePath(String path) {
        path = "lib" + path;
        return path;
    }
}

package exceptions;

import java.io.IOException;

/**
 * Exception representing failure on closing connections.
 */
public class CloseConnectionException extends IOException {

	private String message;

	public CloseConnectionException(String message) {
		this.message = message;
	}
	
	public String getMessage(){
		return this.message;
	}
	
	private static final long serialVersionUID = -8305309183368523254L;
	
}

package exceptions;

import java.io.IOException;

/**
 * Exception representing failure on opening connections.
 */
public class OpenConnectionException extends IOException {
	
	private String message;

	public OpenConnectionException(String message) {
		this.message = message;
	}
	
	public String getMessage(){
		return this.message;
	}

	private static final long serialVersionUID = -4026569087526929494L;
	
}

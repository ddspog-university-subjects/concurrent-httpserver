package parser;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Class responsible to holding default information about the server.
 */
@SuppressWarnings("WeakerAccess")
public class ServerInfo {

    public static final String SP = " ";

    public static final String SERVER_NAME = "Java";
    public static final String SERVER_VERSION = "8";
    public static final String SERVER_OS = "Win";
    public static final String SERVER_OS_ARQ = "64";

    /**
     * Get the date as formatted for HTTP requests.
     * @return the date as formatted for HTTP requests.
     */
    public static String getServerDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat pattern = new SimpleDateFormat(
                "EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
        pattern.setTimeZone(TimeZone.getTimeZone("GMT"));
        return pattern.format(calendar.getTime());
    }

    /**
     * Get the name of server as formatted for HTTP requests.
     * @ the name of server as formatted for HTTP requests.
     */
    public static String getServerName() {
        String serverAlias = SERVER_NAME + "/" + SERVER_VERSION;
        String serverOs = "(" + SERVER_OS + SERVER_OS_ARQ + ")";
        return serverAlias + SP + serverOs;
    }
}

package parser;

import elements.HttpRequest;
import elements.HttpResponse;
import html.DocumentFinder;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Class responsible of create adequate HTTP responses for some requests.
 */
public class ResponseParser {

    /**
     * Create a response for a HTTP request.
     * @param request The request received.
     * @return an HTTP response.
     */
    public static HttpResponse parseRequest(HttpRequest request) {
        HttpResponse response = new HttpResponse();
        response.setVersion(request.getVersion());

        resolveRequest(request, response);

        return response;
    }

    /**
     * Compose response information for any requests.
     * @param request The request received.
     * @param response The response to fill with information.
     */
    private static void resolveRequest(HttpRequest request, HttpResponse response) {
        switch(request.getMethod()){
            case "GET":
                resolveGet(request, response);
                break;
            default:
                response.setStatus("404");
                response.setReason("Not Found");
                break;
        }

        response.addHeader("Date", ServerInfo.getServerDate());
        response.addHeader("Server", ServerInfo.getServerName());
    }

    /**
     * Compose response information for GET requests.
     * @param request The request received.
     * @param response The response to fill with information.
     */
    private static void resolveGet(HttpRequest request, HttpResponse response) {
        switch (request.getPath()) {
            case "/":
                try {
                    Charset encoding = Charset.defaultCharset();
                    String  html = DocumentFinder.getHTML("/", encoding);

                    response.setBody(html);
                    response.addHeader("Content-Length", html.getBytes(encoding).length + "");
                    response.addHeader("Content-Type", "text/html; charset=" + encoding.displayName());

                    response.setStatus("200");
                    response.setReason("OK");
                } catch (IOException e) {
                    response.setStatus("404");
                    response.setReason("Not Found");
                }
                break;
            default:
                response.setStatus("404");
                response.setReason("Not Found");
                break;
        }
    }
}

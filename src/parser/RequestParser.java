package parser;

import elements.HttpRequest;
import workers.ClientConnection;

import java.io.IOException;

/**
 * Class responsible for parsing Http requests as strings and converting to HttpRequest objects.
 */
public class RequestParser {

    /**
     * Parse an online request coming from connection.
     * @param con the server connection waiting for requests.
     * @return the HttpRequest object parsed.
     */
    public static HttpRequest parseRequest(ClientConnection con) {
        HttpRequest request = new HttpRequest();
        readHeader(con, request);
        if(request.hasHeader("Content-Length") || request.hasHeader("Transfer-Encoding")){
            readBody(con, request);
        }
        return request;
    }

    /**
     * Parse an header from online connection.
     * @param con the server connection waiting for requests.
     * @param request the request object to fill header information.
     */
    private static void readHeader(ClientConnection con, HttpRequest request) {
        try {
            StringBuilder headerContent = new StringBuilder();

            String temp = ".";
            while(!temp.equals("")){
                temp = con.in.readLine();
                headerContent.append(temp).append("\n");
            }

            String[] header = headerContent.toString().split("\n");

            // Parse the Request-Line
            String [] tokens = header[0].split(" ");
            request.setMethod(tokens[0]);
            request.setPath(tokens[1]);
            request.setVersion(tokens[2]);

            // Parse the Headers
            int i = 1;
            while (i < header.length){
                temp = header[i++];
                request.addHeader(temp);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Parse a body part from online connection.
     * @param con the server connection waiting for requests.
     * @param request the request object to fill body information.
     */
    private static void readBody(ClientConnection con, HttpRequest request) {
        try {
            String temp = ".";
            while(!temp.equals("")){
                temp = con.in.readLine();
            }

            String[] body = temp.split("\n");

            int i = 0;
            while (i < body.length){
                temp = body[i++];
                request.addToBody(temp);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

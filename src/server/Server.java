
package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.time.Duration;
import java.time.Instant;

/**
 * Class that implements the Server that this program run.
 */
public class Server implements Runnable {

	// Port to listen new requests.
	private int port;

	// Manager responsible for multitasking.
	private Manager manager;

	// Duration of server operations.
	private long duration;

	/**
     * Create the Server to be run without time restriction.
     * @param port The port where this server will listen requests.
     */
	public Server(int port){
		this.port = port;

		this.manager = new Manager();

	}

	/**
	 * Starts server.
	 */
	@Override
	public void run() {
		Instant startTime = Instant.now();
		printInitMessage();
		connect();
		this.duration = Duration.between(startTime, Instant.now()).toNanos();
		printClosingReport();
	}

	/**
	 * Print an informative startup server message.
	 */
	private void printInitMessage() {
		String message = "HTTP v1.0.0 Server starting up...\n" +
				"Listening on http://localhost:" + this.port + "/\n" +
				"Hit Ctrl-Z to quit.";
		System.out.println(message);
	}

	private void printClosingReport() {
		double serverDuration = this.duration / 1000000000.0;
		double average = this.manager.getAnsweredRequests() / serverDuration;

		String message = "HTTP v1.0.0 Server closing...\n" +
				"Server statistics:\n" +
				"\tDuration of server operations: " + serverDuration + " seconds.\n" +
				"\tRequests answered: " + this.manager.getAnsweredRequests() + " requests.\n" +
				"\tRating of server: " + average + " requests per second.\n";
		System.out.println(message);
	}

	/**
	 * Connect the server on its port.
	 */
	private void connect() {
		try (ServerSocket socket = new ServerSocket(this.port)){
			this.manager.setServer(socket);
			this.manager.work();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Main function that starts a server given the port to it.
	 * @param args Contain the port used to connect server.
	 */
	public static void main(String[] args) {
		int argPort = Integer.parseInt(args[0]);

		Server serv = new Server(argPort);
		serv.run();
	}
	
}

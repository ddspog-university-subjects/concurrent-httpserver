package server;

import elements.ConnectionStatus;
import workers.Operator;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.Semaphore;

/**
 * Class responsible to allocate new Operators to attend new requests.
 */
@SuppressWarnings({"WeakerAccess", "FieldCanBeLocal"})
public class Manager implements AutoCloseable {

    // Limit of Operators running.
    private final int MAX_OPERATORS = 4;
    private final ExecutorService executor;
    private final Semaphore semaphore;

    private volatile ConnectionStatus status;

    // Socket responsible to receive new requests.
    private ServerSocket server;

    // Count answered requests on this server.
    private int answeredRequests;

    /**
     * Main constructor.
     */
    public Manager() {
        this.executor = Executors.newFixedThreadPool(MAX_OPERATORS);
        this.semaphore = new Semaphore(MAX_OPERATORS);
    }

    /**
     * Set server for manager to work.
     * @param socket the server socket where manager will connect.
     */
    public void setServer(ServerSocket socket){
        this.server = socket;
    }

    /**
     * Get server for manager to work.
     * @return the server socket where manager will connect.
     */
    public ServerSocket getServer() {
        return server;
    }

    /**
     * Start operating on server. Allocates operators to listen for requests.
     * @throws InterruptedException when thread interrupted during wait.
     */
    public void work() throws InterruptedException {
        this.status = ConnectionStatus.OPERATING;

        while(this.status == ConnectionStatus.OPERATING) {
            try {
                hire();
                semaphore.acquire();
            } catch (Exception e) {
                semaphore.release();
            }
        }

        this.executor.shutdownNow();
    }

    /**
     * Allocate a new operator to listen for new requests, if there are vacancies.
     */
    private void hire() throws InterruptedException {
        Operator operator = new Operator(this);
        this.executor.submit(() -> {
            operator.run();
            incrementAnsweredRequests();
        });
    }

    /**
     * Notify vacancy of operators to listen for requests.
     */
    public synchronized void notifyVacancy() {
        semaphore.release();
    }

    /**
     * Increment answered requests count.
     */
    private synchronized void incrementAnsweredRequests() {
        this.answeredRequests++;
    }

    /**
     * Get the number of anwered requests on this server.
     * @return the number of anwered requests on this server.
     */
    public int getAnsweredRequests() {
        return this.answeredRequests;
    }

    @Override
    public void close() {
        this.status = ConnectionStatus.CLOSED;
        this.executor.shutdownNow();
        this.semaphore.release();
    }
}
